package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char CurrenPlayer = 'X';
    static int row, col, position;

    static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    static void printTable() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    static void printStartgame() {
        System.out.println("Let's start the game!");
    }

    static void inputMove() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Player " + CurrenPlayer + ", enter your move (1-9): ");
            int position = sc.nextInt();
            int row = (position - 1) / 3;
            int col = (position - 1) % 3;
            if (position >= 1 && position <= 9 && table[row][col] == '-') {
                table[row][col] = CurrenPlayer;
                break;
            }
            printTable();
        }
    }

    public static void switchPlayer() {
        CurrenPlayer = (CurrenPlayer == 'X') ? 'O' : 'X';
    }

    public static boolean BoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkWin() {
        return checkRowWin() || checkColWin() || checkDiagonalWin();
    }

    private static boolean checkRowWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] != '-' && table[i][0] == table[i][1] && table[i][1] == table[i][2]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkColWin() {
        for (int i = 0; i < 3; i++) {
            if (table[0][i] != '-' && table[0][i] == table[1][i] && table[1][i] == table[2][i]) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonalWin() {
        return (table[0][0] != '-' && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[0][2] != '-' && table[0][2] == table[1][1] && table[1][1] == table[2][0]);
    }
    public static void resetGame() {
    table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    CurrenPlayer = 'X';
    }

    public static void main(String[] args) {
        printWelcome();
        System.out.println("Player 1: X");
        System.out.println("Player 2: O");
        printStartgame();
        boolean continueGame = true;
        while (continueGame) {
        while (true) {
            printTable();
            inputMove();
            if (checkWin()) {
                System.out.println("Player " + CurrenPlayer + " wins!");
                printTable();

                break;
            }
            if (BoardFull()) {
                System.out.println("It's a draw!");
                printTable();

                break;
            }

            switchPlayer();
        }
        System.out.print("Do you want to continue playing? (Y/N): ");
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        char character = input.charAt(0);
        
        switch (character) {
            case 'Y':
            case 'y':
                resetGame();
                break;
            case 'N':
            case 'n':
                continueGame = false;
                break;
            default:
                resetGame();
                break;
        }
    }
}
}
      